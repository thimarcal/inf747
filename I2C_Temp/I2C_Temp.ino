// Wire Slave Receiver
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Receives data as an I2C/TWI slave device
// Refer to the "Wire Master Writer" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// Porta do pino de sinal do DS18B20
#define ONE_WIRE_BUS 3
 
// Define uma instancia do oneWire para comunicacao com o sensor
OneWire oneWire(ONE_WIRE_BUS);
 
DallasTemperature sensors(&oneWire);
float tempC;

void setup() {
  Wire.begin(0x66);                // join i2c bus with address #0x23
  Wire.onRequest(requestEvent);
  Serial.begin(115200);           // start serial for output
  sensors.begin();
}

void loop() {
  delay(100);
  sensors.requestTemperatures();
  tempC = sensors.getTempCByIndex(0);
}

void requestEvent() {
  // Le a informacao do sensor
  //float tempC = sensors.getTempCByIndex(0);
  byte byteArray[sizeof(float)];

  tempC = tempC*100;
  byteArray[0] = (int)tempC / 1000;
  tempC = (int)tempC % 1000;
  byteArray[1] = (int)tempC / 100;
  tempC = (int)tempC % 100;
  byteArray[2] = (int)tempC / 10;
  byteArray[3] = (int)tempC % 10;
  
  // convert from an unsigned long int to a 4-byte array
  
  Wire.write(byteArray, sizeof(float));
}
