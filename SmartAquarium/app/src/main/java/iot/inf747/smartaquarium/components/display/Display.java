package iot.inf747.smartaquarium.components.display;

import java.util.Locale;

import static java.lang.Thread.sleep;

/**
 * Class responsible for updating the project Display (LCD Device).
 */
public class Display {

    private LcdI2C lcd;

    private String waterTemp;
    private String targetTemp;
    private String light;
    private String envTemp;
    private boolean connected = true;
    private boolean dirty;

    private Thread refreshThread;

    private final Object lockObject;

    public Display() {
        lockObject = new Object();
        refreshThread = new Thread(new RefreshJob());
    }

    public boolean attachToHardware(String i2cName, int i2cAddress) {
        lcd = new LcdI2C(i2cName, i2cAddress, 16, 2);
        lcd.begin();
        lcd.clear();

        refreshThread.start();

        return true;
    }

    public void setWaterTemp(double value) {
        String str = String.format(Locale.getDefault(),
                "%.1f", value);
        if (!str.equals(waterTemp)) {
            waterTemp = str;
            synchronized (lockObject) {
                dirty = true;
                lockObject.notify();
            }
        }
    }

    public void setStateConnected(boolean connected) {
        if (this.connected != connected) {
            this.connected = connected;
            synchronized (lockObject) {
                dirty = true;
                lockObject.notify();
            }
        }
    }

    public void setTargetTemp(double value) {
        String str = String.format(Locale.getDefault(),
                "%.1f", value);
        if (!str.equals(targetTemp)) {
            targetTemp = str;
            synchronized (lockObject) {
                dirty = true;
                lockObject.notify();
            }
        }
    }

    public void setEnvTemp(double value) {
        String str = String.format(Locale.getDefault(),
                "%.1f", value);
        if (!str.equals(envTemp)) {
            envTemp = str;
            synchronized (lockObject) {
                dirty = true;
                lockObject.notify();
            }
        }
    }


    public void setLight(double value) {
        String str = String.format(Locale.getDefault(),
                "%.0f", value);
        if (!str.equals(light)) {
            light = str;
            synchronized (lockObject) {
                dirty = true;
                lockObject.notify();
            }
        }
    }

    private void displayData() {
        if (connected) {
            //lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("A " + envTemp + "C  ");
            lcd.print(" L " + light + "%  ");
        } else {
            //lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("OFF-LINE!       ");
        }
        lcd.setCursor(0, 1);
        lcd.print("W " + waterTemp + "C  ");
        lcd.print(" T " + targetTemp + "C  ");
    }

    private class RefreshJob implements Runnable {

        @Override
        public void run() {

            while (true) {
                try {
                    synchronized (lockObject) {
                        lockObject.wait();
                        if (dirty) {
                            dirty = false;
                            displayData();
                        }
                    }
                    sleep(150);

                } catch (InterruptedException e) {

                }
            }
        }
    }


}
