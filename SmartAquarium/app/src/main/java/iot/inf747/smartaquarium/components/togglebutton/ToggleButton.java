package iot.inf747.smartaquarium.components.togglebutton;

import java.util.ArrayList;
import java.util.List;

import iot.inf747.smartaquarium.components.button.Button;

/**
 * Class implementing toggle buttons.
 */
public class ToggleButton extends Button {

    private long mLastStateChangedOn;

    private static final long TOGGLE_DELAY_IN_MS = 2000;

    private final List<ToggleButtonListener> listeners;

    public enum State {
        ON,
        OFF
    }

    private State mCurrentState;

    public ToggleButton() {
        super();
        mCurrentState = State.OFF;
        listeners = new ArrayList<>();

    }

    @Override
    public void onGpioEdge(boolean newValue) {
        if (newValue && getState() == State.OFF) {
            mCurrentState = State.ON;
            notifyListeners(mCurrentState);
            mLastStateChangedOn = System.currentTimeMillis();
        }
        if (!newValue && getState() == State.ON) {
            if (System.currentTimeMillis() - mLastStateChangedOn > TOGGLE_DELAY_IN_MS) {
                mCurrentState = State.OFF;
                notifyListeners(mCurrentState);
            } else {
                mLastStateChangedOn = 0;
            }
        }
    }


    public State getState() {
        return mCurrentState;
    }

    public void setInitialState(State initialState) {
        mCurrentState = initialState;
    }

    public void addListener(ToggleButtonListener listener) {
        listeners.add(listener);
    }

    public void removeListener(ToggleButtonListener listener) {
        listeners.remove(listener);
    }

    private void notifyListeners(State newState) {
        for (ToggleButtonListener listener : listeners) {
            listener.onStateChanged(newState);
        }
    }

    public void close() {
        super.close();
        listeners.clear();
    }

}
