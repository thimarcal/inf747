package iot.inf747.smartaquarium;

/**
 * Hardware Constants used to map the hardware devices and their physical ports,
 * addresses and configuration values.
 */
public class HardwareConstants {

    public static final String LIGHT_BUTTON_PIN_NAME = "IO2";

    public static final String LIGHT_RELAY_PIN_NAME = "IO17";

    public static final String HEATER_RELAY_PIN_NAME = "IO5";

    public static final String TEMP_INC_BUTTON_PIN_NAME = "IO3";

    public static final String TEMP_DEC_BUTTON_PIN_NAME = "IO6";

    public static final String FEEDER_BUTTON_PIN_NAME = "IO15";

    public static final String I2C_NAME = "I2C6";

    public static final int LCD_I2C_ADDRESS = 0x27;

    public static final String ADC_SPI_ADDRESS = "SPI2.1";

    public static final String STEPPER_PIN_2 = "IO8";
    public static final String STEPPER_PIN_3 = "IO9";
    public static final String STEPPER_PIN_4 = "IO16";
    public static final String STEPPER_PIN_1 = "IO4";
}
