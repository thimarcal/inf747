package iot.inf747.smartaquarium.components.digitalactuator;

import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;

/**
 * Class used to handle actuators connected to a digital GPIO port.
 */
public class DigitalActuator {

    private Gpio mGpio;

    private boolean mEnabled = false;

    private final static String TAG = "DigitalActuator";

    public boolean attachToHardware(String gpioPin) {
        boolean result;

        PeripheralManagerService service = new PeripheralManagerService();
        try {
            mGpio = service.openGpio(gpioPin);
            mGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            result = true;
        } catch (IOException e) {
            Log.e(TAG, "Error on PeripheralIO API", e);
            result = false;
        }
        return result;
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public boolean setEnabled(boolean enabled) {
        boolean res = true;
        try {
            mGpio.setValue(enabled);
            mEnabled = enabled;
        } catch (IOException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    public boolean close() {
        boolean res = true;
        try {
            mGpio.close();
        } catch (IOException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }
}
