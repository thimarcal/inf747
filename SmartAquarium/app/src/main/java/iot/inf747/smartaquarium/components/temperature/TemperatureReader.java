package iot.inf747.smartaquarium.components.temperature;

import com.google.android.things.pio.SpiDevice;

import java.io.IOException;

import iot.inf747.smartaquarium.components.AdcDevice;

/**
 * Driver for the Water Temperature Sensor device over SPI interface.
 */
public class TemperatureReader {
    private static TemperatureReader mTemperatureReader;
    private SpiDevice mSpiDevice;

    public static TemperatureReader getInstance() {
        if (null == mTemperatureReader) {
            mTemperatureReader = new TemperatureReader();
        }

        return mTemperatureReader;
    }

    private TemperatureReader() {
        AdcDevice component = AdcDevice.getInstance();

        mSpiDevice = component.getSpiDevice();
    }

    public double getTemperature() {

        byte[] data = new byte[3];
        final byte[] response = new byte[3];
        data[0] = 1;
        data[1] = (byte) (8 << 4); //1000000
        data[2] = 0;
        int RawADC = 0;

        long Resistance;
        double Temp = 0.0;

        try {
            if (null != mSpiDevice) {
                mSpiDevice.transfer(data, response, 3);
                RawADC = (response[1] << 8) & 0b1100000000; //merge data[1] & data[2] to get result
                RawADC |= (response[2] & 0xff);

                // Calculate temperature based on Thermistor Arduino Lib
                // Assuming a 10k Thermistor.  Calculation is actually: Resistance = (1024/ADC)
                if (RawADC != 0) {
                    Resistance = ((10240000 / RawADC) - 10000);
                } else
                    Resistance = 0;


                /*
                 * Utilizes the Steinhart-Hart Thermistor Equation:
                 * Temperature in Kelvin = 1 / {A + B[ln(R)] + C[ln(R)]^3}
                 * where A = 0.001129148, B = 0.000234125 and C = 8.76741E-08
                 */
                Temp = Math.log(Resistance);
                Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
                Temp = Temp - 273.15;  // Convert Kelvin to Celsius

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Temp;
    }

    public void close() {
        mSpiDevice = null;
        mTemperatureReader = null;
    }
}
