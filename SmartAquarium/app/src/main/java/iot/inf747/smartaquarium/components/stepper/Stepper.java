package iot.inf747.smartaquarium.components.stepper;

import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Driver code for the Stepper device with ULN2003 driver connected with 4 GPIOs ports.
 */
public class Stepper {

    private Gpio mGpio1, mGpio2, mGpio3, mGpio4;

    private final static String TAG = "Stepper";

    private int mStepNumber;
    private int mDirection;
    private long mNumberOfSteps;
    private long mStepDelay;
    private int mStepsTotal;

    private Thread mJobThread;

    public Stepper(int numberOfSteps, int speed) {
        mStepNumber = 0;    // which step the motor is on
        mDirection = 0;      // motor direction
        mNumberOfSteps = numberOfSteps; // total number of steps for this motor
        setSpeed(speed);

        mJobThread = new Thread(new Job());

    }

    public boolean attachToHardware(String gpioPin1, String gpioPin2, String gpioPin3, String gpioPin4) {

        boolean result;

        PeripheralManagerService service = new PeripheralManagerService();
        try {
            mGpio1 = service.openGpio(gpioPin1);
            mGpio1.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);

            mGpio2 = service.openGpio(gpioPin2);
            mGpio2.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);

            mGpio3 = service.openGpio(gpioPin3);
            mGpio3.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);

            mGpio4 = service.openGpio(gpioPin4);
            mGpio4.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            result = true;
        } catch (IOException e) {
            Log.e(TAG, "Error on PeripheralIO API", e);
            result = false;
        }

        return result;

    }

    public void setSpeed(long speed) {
        mStepDelay = 60L * 1000L * 1000L / mNumberOfSteps / speed;
    }

    public void step(int steps) {
        mStepsTotal = steps;
        if (!mJobThread.isAlive()) {
            mJobThread.start();
        }
    }

    public void interrupt() {
        if (mJobThread.isAlive()) {
            mJobThread.interrupt();
        }
    }


    private void stepMotor(int step) throws IOException {
        switch (step) {
            case 0:  // 1010
                mGpio1.setValue(true);
                mGpio2.setValue(false);
                mGpio3.setValue(true);
                mGpio4.setValue(false);

                break;
            case 1:  // 0110
                mGpio1.setValue(false);
                mGpio2.setValue(true);
                mGpio3.setValue(true);
                mGpio4.setValue(false);
                break;
            case 2:  //0101
                mGpio1.setValue(false);
                mGpio2.setValue(true);
                mGpio3.setValue(false);
                mGpio4.setValue(true);
                break;
            case 3:  //1001
                mGpio1.setValue(true);
                mGpio2.setValue(false);
                mGpio3.setValue(false);
                mGpio4.setValue(true);
                break;
        }
    }


    private class Job implements Runnable {

        @Override
        public void run() {
            try {
                int steps_left = Math.abs(mStepsTotal);  // how many steps to take

                // determine direction based on whether steps_to_mode is + or -:
                if (mStepsTotal > 0) {
                    mDirection = 1;
                }
                if (mStepsTotal < 0) {
                    mDirection = 0;
                }

                Log.d(TAG, "steps_left " + steps_left + "  mDirection " + mDirection + " mStepNumber " + mStepNumber);

                // decrement the number of steps, moving one step each time:
                while (steps_left > 0) {

                    TimeUnit.MICROSECONDS.sleep(mStepDelay);

                    // move only if the appropriate delay has passed:
                    //if (now - this->last_step_time >= this->step_delay)
                    {
                        // get the timeStamp of when you stepped:
                        //this->last_step_time = now;
                        // increment or decrement the step number,
                        // depending on direction:
                        if (mDirection == 1) {
                            mStepNumber++;
                            if (mStepNumber == 4) {
                                mStepNumber = 0;
                            }
                        } else {
                            if (mStepNumber == 0) {
                                mStepNumber = 4;
                            }
                            mStepNumber--;
                        }
                        // decrement the steps left:
                        steps_left--;

                        try {
                            stepMotor(mStepNumber % 4);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (InterruptedException e) {
                Log.e(TAG, "Stepper Job interrupted");
            }
            try {
                mGpio1.setValue(false);
                mGpio2.setValue(false);
                mGpio3.setValue(false);
                mGpio4.setValue(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
