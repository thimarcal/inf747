package iot.inf747.smartaquarium.components.lightsensor;

import com.google.android.things.pio.SpiDevice;

import java.io.IOException;

import iot.inf747.smartaquarium.components.AdcDevice;

/**
 * Driver for the Light Sensor device over SPI interface.
 */
public class LDRLightSensor {

    private static LDRLightSensor mInstance;
    private SpiDevice spiDevice;

    public static LDRLightSensor getInstance() {
        if (null == mInstance) {
            mInstance = new LDRLightSensor();
        }

        return mInstance;
    }

    private LDRLightSensor() {
        spiDevice = AdcDevice.getInstance().getSpiDevice();
    }

    public double readLightValue() {
        byte[] data = new byte[3];
        final byte[] response = new byte[3];
        data[0] = 1;
        data[1] = (byte) ((8 + 1) << 4); // 8 + channel
        data[2] = 0;
        int RawADC = 0;

        try {
            if (null != spiDevice) {
                spiDevice.transfer(data, response, 3);
                RawADC = (response[1] << 8) & 0b1100000000; //merge data[1] & data[2] to get result
                RawADC |= (response[2] & 0xff);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return (RawADC * 100.0) / 1023.0;
    }
}
