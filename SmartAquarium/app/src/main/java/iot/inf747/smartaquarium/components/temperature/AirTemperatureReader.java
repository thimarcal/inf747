package iot.inf747.smartaquarium.components.temperature;

import android.util.Log;

import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;

import iot.inf747.smartaquarium.HardwareConstants;

/**
 * Driver for the TH02 sensor device over I2C interface.
 */
public class AirTemperatureReader {

    private static final String TAG = "TH02";

    private static final byte TH02_ADDR = 0x40; // device address

    private static final byte TH02_REG_STATUS = 0x00;
    private static final byte TH02_REG_DATA_H = 0x01;
    private static final byte TH02_REG_DATA_L = 0x02;
    private static final byte TH02_REG_CONFIG = 0x03;

    private static final byte TH02_STATUS_RDY_MASK = 0x01;

    private static final byte TH02_CMD_MEASURE_TEMP = 0x11;

    private static AirTemperatureReader instance;

    // I2C Device representing the TH02 instance
    private I2cDevice mI2Cth02;

    private AirTemperatureReader() {
        try {
            PeripheralManagerService managerService = new PeripheralManagerService();
            mI2Cth02 = managerService.openI2cDevice(HardwareConstants.I2C_NAME, TH02_ADDR);
        } catch (IOException e) {
            Log.d(TAG, "Failed to open I2C device: " + e.getMessage());
        }
    }


    public static AirTemperatureReader getInstance() {
        if (null == instance) {
            instance = new AirTemperatureReader();
        }

        return instance;
    }

    public double getTemperature() {
        try {
            // Start a new temperature conversion
            mI2Cth02.writeRegByte(TH02_REG_CONFIG, TH02_CMD_MEASURE_TEMP);

            // Wait until conversion is done
            while (!isReady()) ;

            long temperature = mI2Cth02.readRegByte(TH02_REG_DATA_H);
            temperature <<= 8;
            temperature += mI2Cth02.readRegByte(TH02_REG_DATA_L);
            temperature >>= 2;

            double calculated_temp = (temperature / 32.0) - 50.0;

            Log.i(TAG, "temperature: " + calculated_temp);

            return calculated_temp;
        } catch (IOException ex) {
            return -1;
        }
    }

    private boolean isReady() {
        try {
            byte status = mI2Cth02.readRegByte(TH02_REG_STATUS);

            if ((status & TH02_STATUS_RDY_MASK) > 0) {
                return false; // NOT ready
            } else {
                return true; // ready
            }
        } catch (IOException ex) {
            return false;
        }
    }

}
