package iot.inf747.smartaquarium.components.clickbutton;

/**
 * Interface used to listen the click buttons.
 */
public interface ClickButtonListener {

    /**
     * Method called when the button is pressed or kept pressed.
     */
    void onClick();
}
