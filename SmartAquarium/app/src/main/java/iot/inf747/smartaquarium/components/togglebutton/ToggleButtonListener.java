package iot.inf747.smartaquarium.components.togglebutton;

/**
 * Interface used to listen the toggle button state changes.
 */
public interface ToggleButtonListener {

    /**
     * Method called when the toggle button state changes.
     */
    void onStateChanged(ToggleButton.State newState);
}
