package iot.inf747.smartaquarium;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Calendar;
import java.util.Locale;

import iot.inf747.smartaquarium.cloud.FirebaseDataChangeListener;
import iot.inf747.smartaquarium.cloud.FirebaseHelper;
import iot.inf747.smartaquarium.components.analytics.Analytics;
import iot.inf747.smartaquarium.components.clickbutton.ClickButton;
import iot.inf747.smartaquarium.components.clickbutton.ClickButtonListener;
import iot.inf747.smartaquarium.components.digitalactuator.DigitalActuator;
import iot.inf747.smartaquarium.components.display.Display;
import iot.inf747.smartaquarium.components.lightsensor.LDRLightSensor;
import iot.inf747.smartaquarium.components.stepper.Stepper;
import iot.inf747.smartaquarium.components.temperature.AirTemperatureReader;
import iot.inf747.smartaquarium.components.temperature.TemperatureReader;
import iot.inf747.smartaquarium.components.togglebutton.ToggleButton;
import iot.inf747.smartaquarium.components.togglebutton.ToggleButtonListener;

/**
 * Main class used for the Smart Aquarium project. It is responsible for the initiation of all the
 * devices, connection to the Firebase real-time database and orchestration of all the project
 * components.
 */
public class MainActivity extends AppCompatActivity implements FirebaseDataChangeListener {

    private static final String TAG = "SmartAquariumAT";

    private static boolean LCD_SUPPORTED = true;
    private static boolean FIREBASE_CONNECT = true;
    private static boolean THINGSPEAK_ANALYTICS = true;
    private static boolean STEPPER_CONNECTED = true;

    private static final double MAX_TARGET_TEMP = 30.0;

    private ClickButton mTempIncButton;
    private ClickButton mTempDecButton;
    private ToggleButton mLightButton;

    private DigitalActuator mLightRelay;
    private DigitalActuator mHeaterRelay;

    private Stepper mStepper;

    private AirTemperatureReader mAirTemperatureReader;
    private TemperatureReader mTemperatureReader;
    private LDRLightSensor mLightSensor;

    private double targetTemp = 27;
    private double currentTemp = 0;
    private double lightValue = 0;
    private double currentAirTemp = 0;

    private int countTemp = 0;
    private int countLight = 0;

    private FirebaseHelper mFirebaseHelper;

    private Analytics mAnalytics;

    private static final String DEVICE_NAME = Build.SERIAL;

    private Display display;

    private ConnectivityManager mgr;

    Handler handler = new Handler();
    Runnable updateTemperatureRunner = new UpdateTemperatureRunner();
    Runnable updateLightRunner = new UpdateLightSensorRunner();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupHardware();

        if (FIREBASE_CONNECT) {
            checkNetworkStatus();
            mFirebaseHelper = new FirebaseHelper();
            mFirebaseHelper.init(DEVICE_NAME, this);
        }

        if (THINGSPEAK_ANALYTICS) {
            mAnalytics = new Analytics();
        }

        handler.post(updateTemperatureRunner);
        handler.post(updateLightRunner);

        if (display != null) {
            display.setTargetTemp(targetTemp);
        }

    }

    @Override
    public void onFirebaseDataChangeListener() {
        if (mFirebaseHelper != null) {
            if (targetTemp != mFirebaseHelper.getTargetTemp()) {
                targetTemp = mFirebaseHelper.getTargetTemp();
                if (targetTemp > MAX_TARGET_TEMP) {
                    targetTemp = MAX_TARGET_TEMP;
                    mFirebaseHelper.setTargetTemp(targetTemp);
                }
                Log.i(TAG, "target Temp changed: " + targetTemp);
                onTargetTempChange(0, true);
            }
            if (mFirebaseHelper.isLightState() != mLightRelay.isEnabled()) {
                mLightButton.setInitialState(mFirebaseHelper.isLightState() ? ToggleButton.State.ON :
                        ToggleButton.State.OFF);
                onLightStateChanged(mFirebaseHelper.isLightState() ? ToggleButton.State.ON :
                        ToggleButton.State.OFF, true);
            }
            if (mFirebaseHelper.isFeederRequested()) {
                onFeederTriggered(true);
            }
        }

    }

    private void setupHardware() {
        mTempIncButton = new ClickButton();
        mTempIncButton.attachToHardware(HardwareConstants.TEMP_INC_BUTTON_PIN_NAME);
        mTempIncButton.addListener(new ClickButtonListener() {
            @Override
            public void onClick() {
                onTargetTempChange(0.1, false);
            }
        });

        mTempDecButton = new ClickButton();
        mTempDecButton.attachToHardware(HardwareConstants.TEMP_DEC_BUTTON_PIN_NAME);
        mTempDecButton.addListener(new ClickButtonListener() {
            @Override
            public void onClick() {
                onTargetTempChange(-0.1, false);
            }
        });

        mLightButton = new ToggleButton();
        mLightButton.attachToHardware(HardwareConstants.LIGHT_BUTTON_PIN_NAME);
        mLightButton.addListener(new ToggleButtonListener() {
            @Override
            public void onStateChanged(ToggleButton.State newState) {
                onLightStateChanged(newState, false);
            }
        });

        ClickButton feederButton = new ClickButton(false);
        feederButton.attachToHardware(HardwareConstants.FEEDER_BUTTON_PIN_NAME);
        feederButton.addListener(new ClickButtonListener() {
            @Override
            public void onClick() {
                Log.i(TAG, "Feed them!!!");
                onFeederTriggered(false);
            }
        });

        mTemperatureReader = TemperatureReader.getInstance();
        mLightSensor = LDRLightSensor.getInstance();
        mAirTemperatureReader = AirTemperatureReader.getInstance();

        mLightRelay = new DigitalActuator();
        mLightRelay.attachToHardware(HardwareConstants.LIGHT_RELAY_PIN_NAME);

        mHeaterRelay = new DigitalActuator();
        mHeaterRelay.attachToHardware(HardwareConstants.HEATER_RELAY_PIN_NAME);
        mHeaterRelay.setEnabled(false);

        if (LCD_SUPPORTED) {
            display = new Display();
            display.attachToHardware(HardwareConstants.I2C_NAME, HardwareConstants.LCD_I2C_ADDRESS);
        }

        if (STEPPER_CONNECTED) {
            mStepper = new Stepper(500, 60);
            mStepper.attachToHardware(HardwareConstants.STEPPER_PIN_1, HardwareConstants.STEPPER_PIN_2,
                    HardwareConstants.STEPPER_PIN_3, HardwareConstants.STEPPER_PIN_4);
        }

    }

    private void onLightStateChanged(ToggleButton.State newState, boolean remote) {
        Log.i(TAG, "onLightStateChanged");
        if (mLightRelay.setEnabled(newState == ToggleButton.State.ON)) {
            Log.i(TAG, "Light Enabled: " + (newState == ToggleButton.State.ON));
            if (mFirebaseHelper != null && !remote) {
                mFirebaseHelper.setLightState((newState == ToggleButton.State.ON));
            }
        }
    }

    private void onTargetTempChange(double delta, boolean remote) {
        targetTemp += delta;
        if (targetTemp > MAX_TARGET_TEMP) {
            targetTemp = MAX_TARGET_TEMP;
        }
        Log.i(TAG, String.format(Locale.getDefault(),
                "Temp: %.1f", targetTemp));
        checkHeaterState();
        if (mFirebaseHelper != null) {
            if (!remote) {
                mFirebaseHelper.setTargetTemp(targetTemp);
            }
            mFirebaseHelper.setHeaterState(mHeaterRelay.isEnabled());
        }
        if (display != null) {
            display.setTargetTemp(targetTemp);
        }
    }

    private void onFeederTriggered(boolean remote) {
        Log.i(TAG, "onFeederTriggered");

        if (mStepper != null) {
            mStepper.step(2100);
        }
        if (mFirebaseHelper != null) {
            mFirebaseHelper.setFeederLastEvent(Calendar.getInstance().getTime().getTime());
            if (remote) {
                mFirebaseHelper.setFeederRequested(false);
            }
        }
    }


    private void checkHeaterState() {
        if ((currentTemp < targetTemp) && !mHeaterRelay.isEnabled()) {
            mHeaterRelay.setEnabled(true);
        } else if ((currentTemp > targetTemp) && mHeaterRelay.isEnabled()) {
            mHeaterRelay.setEnabled(false);
        }
    }

    private boolean checkNetworkStatus() {
        boolean result;
        if (mgr == null) {
            mgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        }
        NetworkInfo netInfo = mgr.getActiveNetworkInfo();
        result = (netInfo != null && netInfo.isConnectedOrConnecting());
        if (display != null) {
            display.setStateConnected(result);
        }
        Log.i(TAG, "Connected: " + result);

        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mTempDecButton.close();
        mTempIncButton.close();
        mLightButton.close();
        mLightRelay.close();

        mTempIncButton.close();

        handler.removeCallbacks(updateTemperatureRunner);
        if (null != mTemperatureReader) {
            mTemperatureReader.close();
        }
        Log.d(TAG, "onDestroy");
    }

    private class UpdateLightSensorRunner implements Runnable {

        @Override
        public void run() {
            if (null != mLightSensor) {
                lightValue = mLightSensor.readLightValue();
                if (display != null) {
                    display.setLight(lightValue);
                }

                if (null != mFirebaseHelper && (countLight++ % 5 == 0)) {
                    mFirebaseHelper.setEnvLight(lightValue);
                }
            }

            handler.postDelayed(this, 2000);
        }
    }

    /**
     * runner to periodically read temperature
     */
    private class UpdateTemperatureRunner implements Runnable {

        @Override
        public void run() {
            if (null != mTemperatureReader) {
                currentTemp = mTemperatureReader.getTemperature();
            }
            if (null != mAirTemperatureReader) {
                currentAirTemp = mAirTemperatureReader.getTemperature();
            }
            if (display != null) {
                display.setEnvTemp(currentAirTemp);
                display.setWaterTemp(currentTemp);
            }
            if (countTemp++ % 5 == 0) {
                checkHeaterState();
                checkNetworkStatus();
                if (mFirebaseHelper != null) {
                    mFirebaseHelper.setWaterTemp(currentTemp);
                    mFirebaseHelper.setEnvTemperature(currentAirTemp);
                }
            }

            logTemperature();

            handler.postDelayed(this, 2000);
        }

        /**
         * Send analytics event log of the read temperatures to ThingsSpeak.
         */
        private void logTemperature() {
            if (null != mAnalytics) {
                Analytics.Bundle bundle = Analytics.newBundle();
                bundle.setAirTemperature(currentAirTemp);
                bundle.setWaterTemperature(currentTemp);
                bundle.setLightValue(lightValue);
                mAnalytics.send(bundle);

                Log.i(TAG, "logTemperature - sent data to ThingsSpeak [" + bundle + "]");
            }
        }
    }

}
