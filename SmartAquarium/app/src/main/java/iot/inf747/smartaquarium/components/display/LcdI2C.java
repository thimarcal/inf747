package iot.inf747.smartaquarium.components.display;

import android.util.Log;

import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Driver for the LCD device over I2C interface.
 */
public class LcdI2C {

    private static final String TAG = "LcdI2C";

    // Test Bytes for I2C Connection
    private static final byte TEST_BYTES = 0x0;

    // Constant Bits
    private static final byte En = 0x04; // Enable bit 00000100
    private static final byte Rw = 0x02; // Read/Write bit 00000010
    private static final byte Rs = 0x01; // Register select bit 00000001

    // I2C LCD Messages
    private static final byte BACKLIGHT_ON = 0x08;
    private static final byte BACKLIGHT_OFF = 0x00;

    // LCD Commands
    private static final byte LCD_CLEARDISPLAY = 0x01;
    private static final byte LCD_RETURNHOME = 0x02;
    private static final byte LCD_ENTRYMODESET = 0x04;
    private static final byte LCD_DISPLAYCONTROL = 0x08;
    private static final byte LCD_CURSORSHIFT = 0x10;
    private static final byte LCD_FUNCTIONSET = 0x20;
    private static final byte LCD_SETCGRAMADDR = 0x40;
    private static final byte LCD_SETDDRAMADDR = (byte) 0x80;

    // flags for display entry mode
    private static final byte LCD_ENTRYRIGHT = 0x00;
    private static final byte LCD_ENTRYLEFT = 0x02;
    private static final byte LCD_ENTRYSHIFTINCREMENT = 0x01;
    private static final byte LCD_ENTRYSHIFTDECREMENT = 0x00;

    // flags for display on/off control
    private static final byte LCD_DISPLAYON = 0x04;
    private static final byte LCD_DISPLAYOFF = 0x00;
    private static final byte LCD_CURSORON = 0x02;
    private static final byte LCD_CURSOROFF = 0x00;
    private static final byte LCD_BLINKON = 0x01;
    private static final byte LCD_BLINKOFF = 0x00;

    // flags for display/cursor shift
    private static final byte LCD_DISPLAYMOVE = 0x08;
    private static final byte LCD_CURSORMOVE = 0x00;
    private static final byte LCD_MOVERIGHT = 0x04;
    private static final byte LCD_MOVELEFT = 0x00;

    // flags for function set
    private static final byte LCD_8BITMODE = 0x10;
    private static final byte LCD_4BITMODE = 0x00;
    private static final byte LCD_2LINE = 0x08;
    private static final byte LCD_1LINE = 0x00;
    private static final byte LCD_5x10DOTS = 0x04;
    private static final byte LCD_5x8DOTS = 0x00;

    PeripheralManagerService managerService = new PeripheralManagerService();

    // I2C Device representing the LCD instance
    private I2cDevice mI2CLcd;

    // LCD Parameters
    private String mI2CName = "";
    private int mI2CAddress = 0;
    private int mColumns;
    private int mLines;

    // Control variables
    private byte mBacklightValue;
    private byte mDisplayFunction;
    private byte mDisplayControl;
    private byte mDisplayMode;

    /**
     * @param i2cName    The name of I2C interface, in Android Things (eg. I2C6)
     * @param i2cAddress The I2C address of the I2C (eg. 0x27)
     * @param columns    Number of columns for each line (eg. 16, 24)
     * @param lines      Number of lines (eg. 2, 4)
     * @Constructor Creates an instance of the LcdI2C by specifying Name and Address
     */
    public LcdI2C(String i2cName, int i2cAddress, int columns, int lines) {
        if (null != i2cName) {
            mI2CName = i2cName;
        }

        mI2CAddress = i2cAddress;
        mColumns = columns;
        mLines = lines;
    }

    /**
     * This method must be called to initialize I2C device
     *
     * @return Success to initialize Device
     */
    public boolean begin() {
        try {
            mI2CLcd = managerService.openI2cDevice(mI2CName, mI2CAddress);

            mDisplayFunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;

            if (mLines > 1) {
                mDisplayFunction |= LCD_2LINE;
            }

            // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
            // according to datasheet, we need at least 40ms after power rises above 2.7V
            // before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
            delay(50);

            // Now we pull both RS and R/W low to begin commands
            expanderWrite(mBacklightValue);    // reset expanderand turn backlight off (Bit 8 =1)
            delay(1000);

            //put the LCD into 4 bit mode
            // this is according to the hitachi HD44780 datasheet
            // figure 24, pg 46

            // we start in 8bit mode, try to set 4 bit mode
            write4bits((byte) (0x03 << 4));
            delayMicroseconds(4500); // wait min 4.1ms

            // second try
            write4bits((byte) (0x03 << 4));
            delayMicroseconds(4500); // wait min 4.1ms

            // third go!
            write4bits((byte) (0x03 << 4));
            delayMicroseconds(150);

            // finally, set to 4-bit interface
            write4bits((byte) (0x02 << 4));

            // set # lines, font size, etc.
            command((byte) (LCD_FUNCTIONSET | mDisplayFunction));

            // turn the display on with no cursor or blinking default
            mDisplayControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
            display();

            // clear it off
            clear();

            // Initialize to default text direction (for roman languages)
            mDisplayMode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

            // Set backlight to On
            backlight(true);

            // set the entry mode
            command((byte) (LCD_ENTRYMODESET | mDisplayMode));

            home();


        } catch (IOException e) {
            Log.d(TAG, "Failed to open I2C device");
            return false;
        }
        return true;
    }

    /**
     * Ends I2C device. After it's called, begin() should be called again
     */
    public void end() {
        try {
            if (null != mI2CLcd) {
                mI2CLcd.close();
            }
        } catch (IOException e) {
            Log.d(TAG, "Failed to Close I2C Device");
        } finally {
            mI2CLcd = null;
        }
    }


    public void backlight(boolean backlight) {
        if (null != mI2CLcd) {
            mBacklightValue = backlight ? BACKLIGHT_ON : BACKLIGHT_OFF;

            expanderWrite((byte) 0x0);
        }
    }

    /********** high level commands, for the user! */
    public void clear() {
        command(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
        try {
            TimeUnit.MICROSECONDS.sleep(2000);  // this command takes a long time!
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void home() {
        command(LCD_RETURNHOME);  // set cursor position to zero
        try {
            TimeUnit.MICROSECONDS.sleep(2000);  // this command takes a long time!
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setCursor(int col, int row) {
        byte row_offsets[] = {0x00, 0x40, 0x14, 0x54};
        if (row > mLines) {
            row = (byte) (mLines - 1);    // we count rows starting w/0
        }
        command((byte) (LCD_SETDDRAMADDR | (col + row_offsets[row])));
    }

    public void print(String message) {
        for (char letter : message.toCharArray()) {
            write((byte) letter);
        }
    }

    /**
     * Utility Methods for writing to LCD
     */

    /*********** mid level commands, for sending data/cmds */

    private void command(byte value) {
        send(value, (byte) 0);
    }

    private int write(byte value) {
        send(value, Rs);
        return 1;
    }


    /************ low level data pushing commands **********/
    private void send(byte value, byte mode) {
        int highnib = value & 0xf0;
        int lownib = (value << 4) & 0xf0;
        write4bits((byte) ((highnib) | mode));
        write4bits((byte) ((lownib) | mode));
    }

    private void write4bits(byte value) {
        expanderWrite(value);
        pulseEnable(value);
    }

    private void expanderWrite(byte _data) {
        byte[] msg = {(byte) (_data | mBacklightValue)};
        if (null != mI2CLcd) {
            try {
                mI2CLcd.write(msg, msg.length);
            } catch (IOException e) {
                Log.d(TAG, "Failed to write to I2C LCD");
            }
        }
    }

    private void pulseEnable(byte _data) {

        try {
            expanderWrite((byte) (_data | En));    // En high
            TimeUnit.MICROSECONDS.sleep(1);        // enable pulse must be >450ns

            expanderWrite((byte) (_data & ~En));    // En low
            TimeUnit.MICROSECONDS.sleep(50);        // commands need > 37us to settle
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // Turn the display on/off (quickly)
    void noDisplay() {
        mDisplayControl &= ~LCD_DISPLAYON;
        command((byte) (LCD_DISPLAYCONTROL | mDisplayControl));
    }

    void display() {
        mDisplayControl |= LCD_DISPLAYON;
        command((byte) (LCD_DISPLAYCONTROL | mDisplayControl));
    }

    // Turns the underline cursor on/off
    void noCursor() {
        mDisplayControl &= ~LCD_CURSORON;
        command((byte) (LCD_DISPLAYCONTROL | mDisplayControl));
    }

    void cursor() {
        mDisplayControl |= LCD_CURSORON;
        command((byte) (LCD_DISPLAYCONTROL | mDisplayControl));
    }

    // Turn on and off the blinking cursor
    void noBlink() {
        mDisplayControl &= ~LCD_BLINKON;
        command((byte) (LCD_DISPLAYCONTROL | mDisplayControl));
    }

    void blink() {
        mDisplayControl |= LCD_BLINKON;
        command((byte) (LCD_DISPLAYCONTROL | mDisplayControl));
    }

    private void delay(int ms) {
        try {
            TimeUnit.MILLISECONDS.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void delayMicroseconds(int us) {
        try {
            TimeUnit.MICROSECONDS.sleep(us);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
