package iot.inf747.smartaquarium;

import com.google.android.things.pio.I2cDevice;

import java.io.IOException;

import nz.geek.android.things.drivers.i2c.BaseI2cDevice;

/**
 * Class to work with Arduino connected to Edison by I2C bus in slave mode to get
 * the ADC sensor values connected to it.
 * <p>
 * IMPORTANT: It was deprecated in the final project proposal.
 */
public class BusBuddy extends BaseI2cDevice {

    /* package */ BusBuddy(I2cDevice device) {
        super(device);
    }

    public static BusBuddy create(int address) {
        return new BusBuddy(getDevice(getBus(), address));
    }

    public void destroy() {
        super.close();
    }

    public float readValue() {
        if (device == null) return 0;

        byte[] buffer = new byte[]{0, 0, 0, 0};

        float value = 0;

        try {
            device.read(buffer, 4);
            value += buffer[0] * 10;
            value += buffer[1];
            value += (float) buffer[2] / 10.0;
            value += (float) buffer[3] / 100.0;

        } catch (IOException e) {
            // ignore
        }
        return value;
    }
}