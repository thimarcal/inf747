package iot.inf747.smartaquarium.components.analytics;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

/**
 * Class responsible for sending data to ThingSpeak.
 */
public class Analytics {

    private static final String API_KEY = "0WNSIY0IE3E22S72";

    public static Bundle newBundle() {
        return new Bundle();
    }

    public void send(Bundle bundle) {
        if (null == bundle || bundle.isEmpty()) {
            throw new IllegalArgumentException("Bundle cannot be null or empty!");
        }

        new DataSender().execute(bundle);
    }

    private static final class DataSender extends AsyncTask<Bundle, Void, Void> {
        @Override
        protected Void doInBackground(Bundle... bundles) {
            for (Bundle bundle : bundles) {
                this.sendBundle(bundle);
            }
            return null;
        }

        private void sendBundle(Bundle bundle) {
            HttpsURLConnection conn = null;
            try {
                Uri.Builder uriBuilder = new Uri.Builder()
                        .scheme("https")
                        .authority("api.thingspeak.com")
                        .path("update")
                        .appendQueryParameter("api_key", API_KEY);

                for (Map.Entry<String, Double> entry : bundle.getDataMapEntries()) {
                    uriBuilder.appendQueryParameter(entry.getKey(), entry.getValue().toString());
                }

                Uri uri = uriBuilder.build();
                conn = (HttpsURLConnection) new URL(uri.toString()).openConnection();
                conn.connect();
                if (200 != conn.getResponseCode()) {
                    Log.e("ANALYTICS", "Things Speak response code: "
                            + conn.getResponseCode() + ", Msg: " + conn.getResponseMessage());
                }
            } catch (MalformedURLException | ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (null != conn) {
                    conn.disconnect();
                }
            }
        }
    }

    public static final class Bundle {
        private Map<String, Double> dataMap = new HashMap<>();

        private Bundle() {
        }

        public void setWaterTemperature(double value) {
            this.dataMap.put("field1", value);
        }

        public void setAirTemperature(double value) {
            this.dataMap.put("field2", value);
        }

        public void setLightValue(double value) {
            this.dataMap.put("field3", value);
        }

        boolean isEmpty() {
            return this.dataMap.isEmpty();
        }

        Set<Map.Entry<String, Double>> getDataMapEntries() {
            return this.dataMap.entrySet();
        }

        @Override
        public String toString() {
            return "Bundle{" +
                    "dataMap=" + dataMap +
                    '}';
        }
    }

}
