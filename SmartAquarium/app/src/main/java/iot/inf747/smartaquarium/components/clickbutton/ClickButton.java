package iot.inf747.smartaquarium.components.clickbutton;

import java.util.ArrayList;
import java.util.List;

import iot.inf747.smartaquarium.components.button.Button;

import static java.lang.Thread.sleep;

/**
 * Button code responsible for handling buttons with repetition functionality. When you press
 * and hold the button, several events are triggered.
 */
public class ClickButton extends Button {

    private static int FIRST_STAGE_DELAY = 1000;
    private static int SECOND_STAGE_DELAY = 300;
    private static int THIRD_STAGE_DELAY = 300;

    private final List<ClickButtonListener> listeners;

    private Thread mHold;

    private boolean mRepetition;

    public ClickButton() {
        this(true);
    }

    public ClickButton(boolean repetition) {
        super();
        listeners = new ArrayList<>();
        mRepetition = repetition;
        mHold = new Thread(new countClickJob());
    }


    @Override
    public void onGpioEdge(boolean newValue) {
        if (newValue) {
            notifyListeners();
            if (isPressed() && mRepetition && !mHold.isAlive()) {
                mHold.start();
            }
        } else {
            if (mHold.isAlive()) {
                mHold.interrupt();
            }
        }
    }

    public void addListener(ClickButtonListener listener) {
        listeners.add(listener);
    }

    public void removeListener(ClickButtonListener listener) {
        listeners.remove(listener);
    }

    private void notifyListeners() {
        for (ClickButtonListener listener : listeners) {
            listener.onClick();
        }
    }

    public void close() {
        super.close();
        listeners.clear();
    }

    private class countClickJob implements Runnable {

        @Override
        public void run() {
            boolean keepRunning = true;
            int count = 0;
            while (keepRunning) {
                try {
                    if (count == 0) {
                        sleep(FIRST_STAGE_DELAY);
                    } else if (count <= 5) {
                        sleep(SECOND_STAGE_DELAY);
                    } else {
                        sleep(THIRD_STAGE_DELAY);
                    }
                    count++;
                    if (isPressed()) {
                        notifyListeners();
                    } else {
                        keepRunning = false;
                    }
                } catch (InterruptedException e) {
                    keepRunning = false;
                }
            }

        }
    }

}
