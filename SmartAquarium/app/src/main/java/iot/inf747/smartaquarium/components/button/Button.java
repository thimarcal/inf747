package iot.inf747.smartaquarium.components.button;

import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.GpioCallback;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;

/**
 * GPIO button code to handle a physical button.
 */
public abstract class Button {

    private final static String TAG = "Button";

    private final long DEBOUNCE_DELAY_IN_MS = 50;

    private Gpio mButtonGpio;
    private long mLastEventTime;


    public boolean attachToHardware(String gpioPin) {
        boolean result;

        PeripheralManagerService service = new PeripheralManagerService();
        try {
            // Create GPIO connection.
            mButtonGpio = service.openGpio(gpioPin);
            mButtonGpio.setDirection(Gpio.DIRECTION_IN);
            mButtonGpio.setEdgeTriggerType(Gpio.EDGE_BOTH);
            mButtonGpio.registerGpioCallback(mCallback);
            result = true;
        } catch (IOException e) {
            Log.e(TAG, "Error on PeripheralIO API", e);
            result = false;
        }
        return result;
    }

    protected Gpio getButtonGpio() {
        return mButtonGpio;
    }

    public boolean isPressed()  {
        boolean pressed = false;
        try {
            pressed = mButtonGpio.getValue();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pressed;
    }


    /**
     * Register the event callback.
     */
    private GpioCallback mCallback = new GpioCallback() {
        @Override
        public boolean onGpioEdge(Gpio gpio) {
            Log.v(TAG, "GPIO changed");
            try {
                boolean value = gpio.getValue();
                if (!value || ((System.currentTimeMillis() - mLastEventTime) > DEBOUNCE_DELAY_IN_MS)) {
                    Button.this.onGpioEdge(value);
                    mLastEventTime = System.currentTimeMillis();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }
    };

    public abstract void onGpioEdge(boolean newValue);


    public void close() {
        mButtonGpio.unregisterGpioCallback(mCallback);
        try {
            mButtonGpio.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
