package iot.inf747.smartaquarium.components;

import com.google.android.things.pio.PeripheralManagerService;
import com.google.android.things.pio.SpiDevice;

import java.io.IOException;

import iot.inf747.smartaquarium.HardwareConstants;

/**
 * Driver code to handle ADC CI connected to the SPI interface.
 */
public class AdcDevice {

    private static AdcDevice mInstance;
    private SpiDevice spiDevice;

    public static AdcDevice getInstance() {
        if (null == mInstance) {
            mInstance = new AdcDevice();
        }
        return mInstance;
    }

    private AdcDevice() {
        PeripheralManagerService service = new PeripheralManagerService();

        try {
            spiDevice = service.openSpiDevice(HardwareConstants.ADC_SPI_ADDRESS);

            spiDevice.setMode(SpiDevice.MODE0);
            spiDevice.setBitsPerWord(8); // 8 BPW
            spiDevice.setFrequency(1000000); // 1MHz
            spiDevice.setBitJustification(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SpiDevice getSpiDevice() {
        return spiDevice;
    }
}
