package inf_747.smartaquariumapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Single Activity of the Android app responsible for provide to interact with the user.
 */
public class MainActivity extends AppCompatActivity implements FirebaseDataChangeListener {

    /**
     * (Intel Edison) Smart Aquarium device name.
     */
    private static final String DEVICE_NAME = "edisond2a9d54a";

    private TextView mEnvTemperature;
    private TextView mEnvLight;

    private Switch mLightSwitch;
    private TextView mWaterTempText;
    private TextView mTargetTempText;
    private SeekBar mTargetTempSeekBar;

    private ImageView mWaterTempImage;

    private TextView mFeederLastEventText;

    private static boolean FIREBASE_CONNECT = true;
    private FirebaseHelper mFirebaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (FIREBASE_CONNECT) {
            FirebaseApp.initializeApp(this);
            mFirebaseHelper = new FirebaseHelper();
            mFirebaseHelper.init(DEVICE_NAME, this);
        }

        mEnvTemperature = findViewById(R.id.tempText);
        mEnvLight = findViewById(R.id.lightText);

        mWaterTempText = findViewById(R.id.WaterTempText);
        mTargetTempText = findViewById(R.id.TargetTempText);

        mTargetTempSeekBar = findViewById(R.id.TargetTempSeekBar);
        mTargetTempSeekBar.setOnSeekBarChangeListener(new TargetSeekerListener());

        mWaterTempImage = findViewById(R.id.WaterTempImage);

        mLightSwitch = findViewById(R.id.LightSwitch);
        mLightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (FIREBASE_CONNECT) {
                    mFirebaseHelper.setLightState(b);
                }

            }
        });

        Button mFeederBtn = findViewById(R.id.FeederButton);
        mFeederBtn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              if (FIREBASE_CONNECT) {
                                                  mFirebaseHelper.setFeederRequested(true);
                                              }
                                          }
                                      }
        );

        mFeederLastEventText = findViewById(R.id.FeederLastEventText);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFirebaseDataChangeListener() {

        mEnvTemperature.setText(String.format(Locale.getDefault(),
                "%.1f °C", mFirebaseHelper.getEnvTemperature()));

        mEnvLight.setText(String.format(Locale.getDefault(),
                "%.1f", mFirebaseHelper.getEnvLight()));

        mWaterTempText.setText(String.format(Locale.getDefault(),
                "%.1f °C", mFirebaseHelper.getWaterTemp()));
        mTargetTempText.setText(String.format(Locale.getDefault(),
                "%.1f °C", mFirebaseHelper.getTargetTemp()));
        mTargetTempSeekBar.setProgress((int) Math.round((mFirebaseHelper.getTargetTemp() - 20) / 0.5));

        if (mFirebaseHelper.isLightState() != mLightSwitch.isChecked()) {
            mLightSwitch.setChecked(mFirebaseHelper.isLightState());
        }
        if (mFirebaseHelper.isHeaterState()) {
            mWaterTempImage.setImageResource(R.drawable.thermometer_water_on);
        } else {
            mWaterTempImage.setImageResource(R.drawable.thermometer_water_off);
        }


        long feederLastEvent = mFirebaseHelper.getFeederLastEvent();
        if (feederLastEvent != 0) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
            mFeederLastEventText.setText("The last event was in " + format.format(new Date(feederLastEvent)));
        } else {
            mFeederLastEventText.setText("");
        }

    }

    private class TargetSeekerListener implements SeekBar.OnSeekBarChangeListener {

        private int current = 20;

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            mTargetTempText.setText(String.format(Locale.getDefault(),
                    "%.1f °C", getTargetTemp(i)));
            current = i;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // nothing to do
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if (FIREBASE_CONNECT) {
                mFirebaseHelper.setTargetTemp(getTargetTemp(current));
            }
        }

        private double getTargetTemp(int i) {
            return i * 0.5 + 20;
        }
    }
}
