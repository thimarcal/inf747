package inf_747.smartaquariumapp;

/**
 * Firebase Data Change Listener.
 */
public interface FirebaseDataChangeListener {

    /**
     * Method called on some data was changed on Firebase real time database.
     */
    void onFirebaseDataChangeListener();

}