package inf_747.smartaquariumapp;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Class responsible for interact wit Firebase real time database, receiving and sending updates.
 */
public class FirebaseHelper {

    private static final String AQUARIUM_TAG = "aquarium";

    private static final String WATER_TEMP = "WaterTemperature";
    private static final String TARGET_TEMP = "TargetTemperature";
    private static final String HEATER_STATUS = "HeaterStatus";
    private static final String LIGHT_STATUS = "LightStatus";

    private static final String ENV_TEMPERATURE = "EnvTemperature";
    private static final String ENV_HUMIDITY = "EnvHumidity";
    private static final String ENV_LIGHT = "EnvLight";

    private static final String FEEDER_REQUESTED = "FeederRequested";
    private static final String FEEDER_LAST_EVENT = "FeederLastEvent";

    private double waterTemp;
    private double targetTemp;
    private boolean heaterState;
    private boolean lightState;

    private double envTemperature;
    private double envHumidity;
    private double envLight;

    private boolean feederRequested;

    private long feederLastEvent;

    private FirebaseDataChangeListener mDataChangeListener;

    private FirebaseDatabase mDB;
    private DatabaseReference mDBReference;

    /**
     * Inits the communication with the firebase.
     *
     * @param deviceName         - the (Edison Device) aquarium name
     * @param dataChangeListener - method called when data was changed on cloud.
     */
    public void init(String deviceName, FirebaseDataChangeListener dataChangeListener) {

        mDataChangeListener = dataChangeListener;

        mDB = FirebaseDatabase.getInstance();
        mDBReference = mDB.getReference().child(AQUARIUM_TAG).child(deviceName);
        mDBReference.addValueEventListener(new FirebaseValueEventListener());
    }

    public double getWaterTemp() {
        return waterTemp;
    }

    public void setWaterTemp(double waterTemp) {
        if (null != mDBReference) {
            mDBReference.child(WATER_TEMP).setValue(waterTemp);
            this.waterTemp = waterTemp;
        }
    }

    public double getTargetTemp() {
        return targetTemp;
    }

    public void setTargetTemp(double targetTemp) {

        if (null != mDBReference) {
            mDBReference.child(TARGET_TEMP).setValue(targetTemp);
            this.targetTemp = targetTemp;
        }
    }

    public boolean isHeaterState() {
        return heaterState;
    }

    public void setHeaterState(boolean heaterState) {
        if (null != mDBReference) {
            mDBReference.child(HEATER_STATUS).setValue(heaterState);
            this.heaterState = heaterState;
        }
    }

    public boolean isLightState() {
        return lightState;
    }

    public void setLightState(boolean lightState) {
        if (null != mDBReference) {
            mDBReference.child(LIGHT_STATUS).setValue(lightState);
            this.lightState = lightState;
        }
    }

    public double getEnvTemperature() {
        return envTemperature;
    }

    public void setEnvTemperature(double envTemperature) {
        if (null != mDBReference) {
            mDBReference.child(ENV_TEMPERATURE).setValue(envTemperature);
            this.envTemperature = envTemperature;
        }
    }

    public double getEnvHumidity() {
        return envHumidity;
    }

    public void setEnvHumidity(double envHumidity) {
        if (null != mDBReference) {
            mDBReference.child(ENV_HUMIDITY).setValue(envHumidity);
            this.envTemperature = envHumidity;
        }
    }

    public double getEnvLight() {
        return envLight;
    }

    public void setEnvLight(double envLight) {
        if (null != mDBReference) {
            mDBReference.child(ENV_LIGHT).setValue(envLight);
            this.envLight = envLight;
        }
    }

    public boolean isFeederRequested() {
        return feederRequested;
    }

    public void setFeederRequested(boolean feederRequested) {

        if (null != mDBReference) {
            mDBReference.child(FEEDER_REQUESTED).setValue(feederRequested);
            this.feederRequested = feederRequested;
        }
    }

    public long getFeederLastEvent() {
        return feederLastEvent;
    }

    public void setFeederLastEvent(long feederLastEvent) {
        if (null != mDBReference) {
            mDBReference.child(FEEDER_LAST_EVENT).setValue(feederLastEvent);
            this.feederLastEvent = feederLastEvent;
        }
    }

    private class FirebaseValueEventListener implements ValueEventListener {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            if (dataSnapshot.hasChild(ENV_TEMPERATURE)) {
                envTemperature = Double.parseDouble(dataSnapshot.child(ENV_TEMPERATURE).getValue().toString());
            }

            if (dataSnapshot.hasChild(ENV_HUMIDITY)) {
                envHumidity = Double.parseDouble(dataSnapshot.child(ENV_HUMIDITY).getValue().toString());
            }

            if (dataSnapshot.hasChild(ENV_LIGHT)) {
                envLight = Double.parseDouble(dataSnapshot.child(ENV_LIGHT).getValue().toString());
            }

            if (dataSnapshot.hasChild(WATER_TEMP)) {
                waterTemp = Double.parseDouble(dataSnapshot.child(WATER_TEMP).getValue().toString());
            }

            if (dataSnapshot.hasChild(TARGET_TEMP)) {
                targetTemp = Double.parseDouble(dataSnapshot.child(TARGET_TEMP).getValue().toString());
            }

            if (dataSnapshot.hasChild(LIGHT_STATUS)) {
                lightState = Boolean.parseBoolean(dataSnapshot.child(LIGHT_STATUS).getValue().toString());
            }

            if (dataSnapshot.hasChild(HEATER_STATUS)) {
                heaterState = Boolean.parseBoolean(dataSnapshot.child(HEATER_STATUS).getValue().toString());
            }

            if (dataSnapshot.hasChild(FEEDER_REQUESTED)) {
                feederRequested = Boolean.parseBoolean(dataSnapshot.child(FEEDER_REQUESTED).getValue().toString());
            }

            if (dataSnapshot.hasChild(FEEDER_LAST_EVENT)) {
                feederLastEvent = Long.parseLong(dataSnapshot.child(FEEDER_LAST_EVENT).getValue().toString());
            }

            if (mDataChangeListener != null) {
                mDataChangeListener.onFirebaseDataChangeListener();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
